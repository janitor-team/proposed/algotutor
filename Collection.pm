# Author:  Chao-Kuei Hung
# For more info, including license, please see doc/index.html

package Collection;
# Abstract Generic Class Collection

use strict;
use Carp;
use vars qw(@ISA);
@ISA = qw(Configurable);

use Configurable;

#sub new {
#    my ($class, %opts) = @_;
#    $class = ref($class) if ref($class);
#    return $class->SUPER::new(%opts);
#}
#

$::Config->{Collection} = {
    -dispheight => 3,
    -compare => sub { return $_[0] <=> $_[1]; },
    -skip => Vector2->new(0.2, 0.1),
    -linear_transform => {
	-scale => Vector2->new(1, 1),
	-offset => Vector2->new(0, 0),
    },
    -node_opts => {},
    -appearance => {
	init    => { -outline=>"Gray40",   -fill=>"Gray85", -thick=>1 },
	alert   => { -outline=>"DarkRed",  -fill=>"LightCoral" },
	focus   => { -outline=>"DarkRed",  -fill=>"LightCoral", -thick=>3},
	pending => { -outline=>"DarkBlue", -fill=>"LightBlue",
		     -thick=>3, -stipple=>"gray25" },
	done    => { -outline=>"DarkGreen",-fill=>"LightGreen",
		     -thick=>3, -stipple=>"gray25" },
	discard => { -outline=>"Yellow3",  -fill=>"Yellow2", -thick=>1},
	hidden  => { -outline=>"hidden",   -fill=>"hidden", -thick=>0 },
    },
};

1;

