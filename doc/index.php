<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- <base href = "http://localhost/~ckhung/"> -->

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator"
  content="HTML Tidy for Linux/x86 (vers 1st March 2003), see www.w3.org" />
  <?php include "../../i/meta.php" ?>

  <title>Algotutor -- Interactive Tutorial for Algorithms and Data
  Structures</title>
</head>

<body>
  
<?php include "header.en.php" ?>
<div id="content">

  <h1>Algotutor -- Interactive Tutorial for Algorithms and Data
  Structures</h1>
  <hr />

  <p>This project is discontinued.</p>

  <ol>
    <li><a href="user.php">User Guide</a> (including <a
    href="user.php#dl">download</a> info)</li>

    <li><a href="q6.en.php">A Sample Quiz Created with the Help
    of Algotutor</a>; <a href="q6.php">Chinese version</a></li>

    <li><a href="algotutor.php">Manual Page for <code class="lit">
    algotutor</code></a></li>

    <li><a href="gen_at_graph.php">Manual Page for <code class="lit">
    gen_at_graph</code></a></li>

    <li><a href="ChangeLog">ChangeLog</a></li>

    <li>License: <a href="gpl.html">GNU General Public License</a></li>

    <li>Author: <a href="http://www.cyut.edu.tw/~ckhung/">Chao-Kuei
    Hung</a></li>
  </ol>

  <hr />

  <p class="center"><img src="bst.png" alt="insertion into a binary
  search tree" /></p>
  <p class="center"><img src="dijk.png" alt="Dijkstra's single-source
  shortest path algorithm on a randomly generated grid-like graph" /></p>

  
<?php include "footer.en.php" ?>
</div>
<?php include "$top[fs]/i/navigator.en.php" ?>
</body>
</html>
