<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>algotutor - an interactive program for observing the intermediate steps 
of algorithms.</title>
<link rev="made" href="mailto:root@localhost" />
</head>

<body style="background-color: white">

<p><a name="__index__"></a></p>
<!-- INDEX BEGIN -->

<ul>

	<li><a href="#name">NAME</a></li>
	<li><a href="#synopsis">SYNOPSIS</a></li>
	<li><a href="#description">DESCRIPTION</a></li>
	<li><a href="#options">OPTIONS</a></li>
	<li><a href="#license">LICENSE</a></li>
	<li><a href="#author">AUTHOR</a></li>
	<li><a href="#see_also">SEE ALSO</a></li>
</ul>
<!-- INDEX END -->

<hr />
<p>
</p>
<h1><a name="name">NAME</a></h1>
<p>algotutor - an interactive program for observing the intermediate steps 
of algorithms.</p>
<p>
</p>
<hr />
<h1><a name="synopsis">SYNOPSIS</a></h1>
<p><strong>algotutor</strong> [<em>OPTION</em>] ... <em>DATA</em> ...</p>
<p>
</p>
<hr />
<h1><a name="description">DESCRIPTION</a></h1>
<p>algotutor is an interactive program for observing the intermediate steps 
of algorithms. The target audience is computer science students and/or anyone 
who studies algorithms and/or data structures. One can create data files in 
plain text format (actually perl anonymous hashes, but one need not care) 
and let algotutor runs through some predefined algorithm. Then one can step 
backward and forward through the execution sequence of the algorithm at 
different levels of details. It requires perl-Tk.</p>
<p><em>DATA</em> is the input data. For the dynamic programming algorithms such
as lcs and matc, please see the respective entries in the following
list; for other algorithms, it is the file name containing the
actual input data.</p>
<p>
</p>
<hr />
<h1><a name="options">OPTIONS</a></h1>
<dl>
<dt><strong><a name="item__2da_algo"><strong>-a</strong> <em>ALGO</em></a></strong><br />
</dt>
<dd>
Runs the algorithm ALGO. Currently ALGO can be one of:
</dd>
<dl>
<dt><strong><a name="item_bst_operations_on_binary_search_trees"><strong>bst</strong> operations on binary search trees</a></strong><br />
</dt>
<dt><strong><a name="item_trees"><strong>rbt</strong> operations on red-black trees (remove() is not implemented yet)</a></strong><br />
</dt>
<dt><strong><a name="item_heap_operations_on_heaps__2d_2d_the_remove_operati"><strong>heap</strong> operations on heaps -- the remove operation on a heap always removes the top element regardless of the argument</a></strong><br />
</dt>
<dt><strong><a name="item_sbs_stack_2dbased_search_on_graphs_2c_a_variant_of"><strong>sbs</strong> stack-based search on graphs, a variant of depth first search</a></strong><br />
</dt>
<dt><strong><a name="item_bfs_breadth_first_search_on_graphs"><strong>bfs</strong> breadth first search on graphs</a></strong><br />
</dt>
<dt><strong><a name="item_prim_prim_27s_minimal_spanning_tree_on_graphs"><strong>prim</strong> Prim's minimal spanning tree on graphs</a></strong><br />
</dt>
<dt><strong><a name="item_dijk_dijkstra_27s_single_2dsource_shortest_path_on"><strong>dijk</strong> Dijkstra's single-source shortest path on graphs</a></strong><br />
</dt>
<dt><strong><a name="item_graphs"><strong>flwa</strong> Floyd-Warshall's all-pair shortest path on graphs (very, very slow)</a></strong><br />
</dt>
<dt><strong><a name="item_dom_2_2ddimensional_point_domination"><strong>dom</strong> 2-dimensional point domination</a></strong><br />
</dt>
<dt><strong><a name="item_graham_graham_27s_scan_for_convex_hull"><strong>graham</strong> Graham's scan for convex hull</a></strong><br />
</dt>
<dt><strong><a name="item_lcs_longest_common_subsequence__2d_2d_it_requires_"><strong>lcs</strong> longest common subsequence -- it requires two strings as
    the command line arguments. For example,
    <code>algotutor -a lcs AGCTATACGATGACT GTCAGTATAGTCATATG</code></a></strong><br />
</dt>
<dt><strong><a name="item_matc_optimal_matrix_chain_multiplication__2d_2d_it"><strong>matc</strong> optimal matrix chain multiplication -- it requires an
    alternating sequence of integers and matrix names as the command
    line arguments. For example, 
    <code>algotutor -a matc 32 A 35 B 24 C 30 D 36 E 25 F 40 G 34 H 35</code>
    means finding the optimal multiplication sequence of the
    chain of matrices: A of size 32 by 35, B of size 35 by 24, ...
    H of size 34 by 35.</a></strong><br />
</dt>
</dl>
<dt><strong><a name="item__2ds_vertex"><strong>-s</strong> <em>VERTEX</em></a></strong><br />
</dt>
<dd>
Use VERTEX as the starting vertex (for sbs, bfs, prim, and dijk)
</dd>
<p></p>
<dt><strong><a name="item__2di_step"><strong>-i</strong> <em>STEP</em></a></strong><br />
</dt>
<dd>
Display step STEP as the initial image.
</dd>
<p></p>
<dt><strong><a name="item__2dd_filename"><strong>-d</strong> <em>FILENAME</em></a></strong><br />
</dt>
<dd>
Dump the picture into FILENAME as a ps file and exit immediately
without going into interactive mode.
</dd>
<p></p></dl>
<p>
</p>
<hr />
<h1><a name="license">LICENSE</a></h1>
<p>This code is distributed under the GNU General Public License</p>
<p>
</p>
<hr />
<h1><a name="author">AUTHOR</a></h1>
<p><strong>Chao-Kuei Hung</strong> ckhung AT ofset DOT org</p>
<p>
</p>
<hr />
<h1><a name="see_also">SEE ALSO</a></h1>
<p>Please see /usr/share/doc/algotutor/doc/ for examples and
the full set of documentations.</p>

</body>

</html>
