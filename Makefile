SRCDIR = ~/active/algotutor
DOCDIR = ~/public_html/p/algotutor
DSTDIR = ~/algotutor
PODSRC = algotutor gen_at_graph

# DATADIR=/usr/share/algotutor/data
# BINDIR=/usr/bin
DATADIR = ./data
BINDIR = .
# .deb and .rpm packagers please note: Please run 
# make test DATADIR=/usr/share/algotutor/data BINDIR=/usr/bin
# or something the like after installing your algotutor package
# so as to make sure that most algorithms run correctly.

dist:	test
	rm red-black-tree.ps
	rm -rf $(DSTDIR)
	cp -LpR . $(DSTDIR)
	for f in $(PODSRC) ; do pod2html $$f > $(DOCDIR)/$$f.shtml ; done ; rm -f *.tmp *.x~~
	cp -LpR $(DOCDIR) $(DSTDIR)/doc
	(cd $(DSTDIR); for f in $(PODSRC) ; do pod2man $$f > doc/$$f.1 ; done ; rm -f *.tmp *.x~~)

test:
	$(BINDIR)/algotutor -a rbt -i 75 -d red-black-tree.ps $(DATADIR)/countries.gr
	$(BINDIR)/algotutor -a graham $(DATADIR)/pts1.gr
	$(BINDIR)/algotutor -a dom $(DATADIR)/pts1.gr
	$(BINDIR)/algotutor -a heap $(DATADIR)/countries.gr
	$(BINDIR)/algotutor -a bst $(DATADIR)/countries.gr
	$(BINDIR)/algotutor -a rbt $(DATADIR)/countries.gr
	$(BINDIR)/algotutor -a sbs $(DATADIR)/trc.gr
	$(BINDIR)/algotutor -a prim $(DATADIR)/randgrid.gr
	$(BINDIR)/algotutor -a dijk $(DATADIR)/tt.gr
	$(BINDIR)/algotutor -a flwa $(DATADIR)/lv.gr
	$(BINDIR)/algotutor -a lcs AGCTATACGATGACT GTCAGTATAGTCATATG
	$(BINDIR)/algotutor -a matc 32 A 35 B 24 C 30 D 36 E 25 F 40 G 34 H 35

